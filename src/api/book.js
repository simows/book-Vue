import request from '../utils/request.js'

export function createBook(book){
  return request({
    url :'/book/create',
    method:'post',
    data:book
  })
}

export function getCategory(book){
  return request({
    url :'/book/category',
    method:'get'
  })
}

export function getBook(bookId){
  return request({
    url :'/book/get',
    method:'get',
    params:{
      bookId
    }
  })
 }

 export function updateBook(book){
  return request({
    url :'/book/update',
    method:'post',
    data:book
  })
 }

 export function listBook(query){
  return request({
    url :'/book/list',
    method:'get',
    params:query
  })
 }
 

 export function deleteBook(id){
  return request({
    url :'/book/delete',
    method:'get',
    params:{id}
  })
 }